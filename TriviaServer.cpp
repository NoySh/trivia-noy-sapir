#include "TriviaServer.h"

#define END_CONNECTION 299


static const unsigned short PORT = 7896;
static const unsigned int IFACE = 0;

TriviaServer::TriviaServer() //c'tor
{
	//add calling db constractor
	_sock = ::socket(AF_INET, SOCK_STREAM, IPPROTO_TCP);
	if (_sock == INVALID_SOCKET)
	{
		throw std::exception(__FUNCTION__ " - socket");
	}

}

TriviaServer::~TriviaServer()//d'tor
{
	while (!_queRecievedMessage.empty()) // deleting the recieved messages queue
	{
		_queRecievedMessage.pop();
	}
	_connectedUsers.clear(); // deleting the list of the users from the map
	TRACE(__FUNCTION__ " closing accepting socket");
	try
	{//closing the socket
		::closesocket(_sock);
	}
	catch (...) {}
}

/*
handling with the messages and getting clients
*/
void TriviaServer::serve()
{
	bindAndListen();
	thread tHandleMessage(this->handleRecievedMessage);
	tHandleMessage.detach();

	while (true)
	{
		accept();
	}
}

/*
start binding to socket and listening.
*/
void TriviaServer::bindAndListen()
{
	struct sockaddr_in sockAddress = { 0 };
	sockAddress.sin_port = htons(PORT);
	sockAddress.sin_family = AF_INET;
	sockAddress.sin_addr.s_addr = IFACE;
	// again stepping out to the global namespace
	if (::bind(_sock, (struct sockaddr*)&sockAddress, sizeof(sockAddress)) == SOCKET_ERROR)
	{
		throw std::exception(__FUNCTION__ " - bind");
	}
	TRACE("binded");

	if (::listen(_sock, SOMAXCONN) == SOCKET_ERROR)
	{

		throw std::exception(__FUNCTION__ " - listen");
	}
	TRACE("listening...");

}

void TriviaServer::accept()
{
	SOCKET client_socket = ::accept(_sock, NULL, NULL);
	if (client_socket == INVALID_SOCKET)
	{
		throw std::exception(__FUNCTION__);
	}
	TRACE("Client accepted !");
	// create new thread for client	and detach from it
	std::thread clientsThread(&TriviaServer::clientHandler, this, client_socket);
	clientsThread.detach();
}

void TriviaServer::clientHandler(SOCKET sock)
{
	RecievedMessage* msg = nullptr;
	try
	{
		// get the first message code
		int msgCode = Helper::getMessageTypeCode(sock);

		while (msgCode != 0 && msgCode != END_CONNECTION)
		{
			msg = buildRecieveMessage(sock, msgCode);
			addRecievedMessage(msg);

			msgCode = Helper::getMessageTypeCode(sock);
		}

		msg = buildRecieveMessage(sock, END_CONNECTION);
		addRecievedMessage(msg);

	}
	catch (const std::exception& e)
	{
		std::cout << "Exception was catch in function clientHandler. socket=" << sock << ", what=" << e.what() << std::endl;
		msg = buildRecieveMessage(sock, END_CONNECTION);
		addRecievedMessage(msg);
	}
	closesocket(sock);
}

void TriviaServer::safeDeleteUser(RecievedMessage * recMsg)
{
	try
	{
		this->handleSignout(recMsg);
		::closesocket(recMsg->getSock()); //closing socket
	}
	catch(...)
	{

	}

}

User * TriviaServer::handleSignin(RecievedMessage * recMsg)
{
	string msg;
	User * currUser = recMsg->getUser();
	string name = currUser->getUsername();
	string pass = Helper::getStringPartFromSocket(recMsg->getSock(), 3); //to change
	//bool matching = DataBase::isUserAndPassMatch(name, pass);
	if (matching)
	{
		if (currUser != nullptr)
		{
			//send fail msg
			msg = "1022";
			currUser->send(msg);

		}
		else
		{
			_connectedUsers.insert(pair<SOCKET, User*>(recMsg->getSock(), currUser)); // adding the user to the connected users list
			//send success to client
			msg = "1020";
			currUser->send(msg);
			return currUser;
		}
	}
	else
	{
		//send fail msg
		msg = "1021";
		currUser->send(msg);
	}
	return nullptr;
}

bool TriviaServer::handleSignup(RecievedMessage * recMsg)
{
	string name = recMsg->getUser()->getUsername();
	string pass = Helper::getStringPartFromSocket(recMsg->getSock(), 3); //to change
	string mail = Helper::getStringPartFromSocket(recMsg->getSock(), 3); //to change
	if (Validator::isPasswordValid(pass))
	{
		if (Validator::isUsernameValid(name))
		{
			/*if(DataBase::isUserExists(name))
			{
				send fail msg - user already exists
			}
			else
			{
				try
				{
					DataBase::addNewUser(name,pass,mail);
					send success msg
				}
				catch(...)
				{
				}
			}*/
		}
		else
		{
			//send fail msg - not good username
		}
	}
	else
	{
		//send fail msg - not good password	
	}
	return false;
}

void TriviaServer::handleRecievedMessage()
{	
	int msgCode = 0;
	SOCKET clientSock = 0;
	string userName;
	while (true)
	{
		try
		{
			unique_lock<mutex> lck(_mtxRecievedMassage);

			// in case the queue is empty.
			if (_queRecievedMessage.empty())
			{
				continue;
			}

			RecievedMessage* currMessage = _queRecievedMessage.front();
			_queRecievedMessage.pop();
			lck.unlock();

			clientSock = currMessage->getSock();
			msgCode = currMessage->getMessageCode();
			if (msgCode == END_CONNECTION || msgCode == 0)
			{
				safeDeleteUser(user->_msg);
			}

			delete currMessage;
		}
		catch (...)
		{

		}
	}
}

void TriviaServer::addRecievedMessage(RecievedMessage * recMsg)
{
	//lock mutex
	_queRecievedMessage.push(recMsg);
	//unlock mutex
}

RecievedMessage * TriviaServer::buildRecieveMessage(SOCKET sock, int msgCode)
{  
	vector<string> msgStringPart;
	string msgData = Helper::getStringPartFromSocket(sock, 4); //to change
	msgStringPart.push_back(msgData);
	RecievedMessage * msg = new RecievedMessage(sock, msgCode, msgStringPart);
	return msg;
}

User * TriviaServer::getUserByName(string name)
{
	map<SOCKET, User*>::iterator it;
	for (it = _connectedUsers.begin(); it != _connectedUsers.end(); it++)
	{
		User* currUser = it->second;
		if (currUser->getUsername() == name)
		{
			return currUser;
		}
	}
	return nullptr;
}

User * TriviaServer::getUserBySocket(SOCKET sock)
{
	return _connectedUsers.at(sock);
}

Room * TriviaServer::getRoomById(int id)
{
	return _roomsList.at(id);
}
