#pragma once
#include <vector>
#include <iostream>
#include "User.h"
using namespace std;


class Room
{
public:
	Room(int id, User * admin, string name, int maxU, int Qno, int Qtime);
	bool joinRoom(User * admin);
	void leaveRoom(User * admin);
	int closeRoom(User * admin);
	vector<User*> getUsers();
	string getUsersListMessage();
	int getQuestionNo();
	int getId();
	string getName(); // the Room name
	User* getAdmin(); // getting the room's admin - added

private:
	vector<User*> _users;
	User* _admin;
	int _maxUsers;
	int _qTime;
	int _qNo;
	string _name;
	int _id;

	string getUsersAsString(vector<User*> users, User* user);
	void sendMessage(string msg);
	void sendMessage(User* user, string msg);
};