#include "User.h"

User::User(string uname, SOCKET sock)
{
	_username = uname;
	_sock = sock;
}

void User::send(string msg)
{
	Helper::sendData(_sock, msg);
}

string User::getUsername()
{
	return _username;
}

SOCKET User::getSocket()
{
	return _sock;
}

Room * User::getRoom()
{
	return _currRoom;
}

bool User::creatRoom(int id, string name, int qNo, int qT, int maxU)
{
	if (this->getRoom() == nullptr)
	{
		_currRoom = new Room(id, this, name, maxU, qNo, qT);
		//send success msg - 114
		return true;
	}
	else
	{
		//send fail msg - 114
		return false;
	}
}

bool User::joinRoom(Room * room)
{
	if (this->_currRoom == nullptr)
	{
		room->joinRoom(this);
		_currRoom = room;
		return true;
	}
	else
	{
		return false;
	}
}

void User::leaveRoom()
{
	if (this->_currRoom != nullptr)
	{
		_currRoom->leaveRoom(this);
		_currRoom = nullptr;
	}
	else
	{
		cout << "Your not in a room"; // for check
	}
}

int User::closeRoom(User * user)
{
	if (user->_currRoom != nullptr)
	{
		user->_currRoom->closeRoom(user);
		this->_currRoom = nullptr;
		return this->_currRoom->getId();
	}
	else
	{
		return -1;
	}
}
