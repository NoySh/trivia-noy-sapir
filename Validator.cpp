#include "Validator.h"

bool Validator::isPasswordValid(string pass)
{
	int isUpper, isLower, isNum = 0;
	if (pass.length() < PASS_SIZE || pass.find(' ') == true)
	{
		return false;
	}
	for (int i = 0; i < pass.length(); i++)
	{
		if (isupper(pass[i]))
		{
			isUpper++;
		}
		else if (islower(pass[i]))
		{
			isLower++;
		}
		else
		{
			isNum++;
		}
	}
	if (isNum == 0 || isUpper == 0 || isLower == 0)
	{
		return false;
	}
	return true;
}

bool Validator::isUsernameValid(string uname)
{ 
	if (isalpha(uname[0]) == false || uname.find(' ') == true || uname.length() == 0)
	{
		return false;
	}
	return true;
}
