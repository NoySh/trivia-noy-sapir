#pragma once
#include <string>
#include <WinSock2.h>

#include "Helper.h"
#include "Room.h"

using namespace std;

class User
{
public:
	User(string uname, SOCKET sock);
	void send(string msg);
	string getUsername();
	SOCKET getSocket();
	Room* getRoom();
	//Game* getGame();
	//void setGame(Game* game);
	void clearRoom();
	bool creatRoom(int id, string name, int qNo, int qT, int maxU);
	bool joinRoom(Room* room);
	void leaveRoom();
	int closeRoom(User * user);
	bool leaveGame();

private:
	string _username;
	Room* _currRoom;
	//Game* _currGame;
	SOCKET _sock;
};