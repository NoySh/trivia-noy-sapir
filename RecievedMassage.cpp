#include "RecievedMassage.h"

SOCKET RecievedMessage::getSock()
{
	return _sock;
}

int RecievedMessage::getMessageCode()
{
	return _msgCode;
}

vector<string> RecievedMessage::getValues()
{
	return _values;
}

User * RecievedMessage::getUser()
{
	return _user;
}
