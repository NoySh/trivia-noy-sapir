#pragma once

#include <iostream>
#include <map>
#include <WinSock2.h>
#include <queue>
#include <thread>
#include <mutex>
#include <utility> 
#include "User.h"
#include "RecievedMassage.h"
#include "Validator.h"

class TriviaServer
{
public:
	TriviaServer();
	~TriviaServer();
	void serve();
private:
	SOCKET _sock;
	map<SOCKET, User*> _connectedUsers;
	//DataBase _db;
	map<int, Room*> _roomsList;
	mutex _mtxRecievedMassage;
	queue<RecievedMessage*> _queRecievedMessage;
	static int _roomIdSequence;
	
	void bindAndListen();
	void accept();
	void clientHandler(SOCKET sock);
	void safeDeleteUser(RecievedMessage* recMsg);

	User* handleSignin(RecievedMessage* recMsg);
	bool handleSignup(RecievedMessage* recMsg);
	void handleSignout(RecievedMessage* recMsg);
	void handleLeaveGame(RecievedMessage* recMsg);
	void handleStartGame(RecievedMessage* recMsg);
	void handlePlayerAnswer(RecievedMessage* recMsg);
	bool handleCreateRoom(RecievedMessage* recMsg);
	bool handleCloseRoom(RecievedMessage* recMsg);
	bool handleJoinRoom(RecievedMessage* recMsg);
	bool handleLeaveRoom(RecievedMessage* recMsg);
	void handleGetUsersInRoom(RecievedMessage* recMsg);
	void handleGetRooms(RecievedMessage* recMsg);
	void handleGetBestScores(RecievedMessage* recMsg);
	void handleGetPersonalStatus(RecievedMessage* recMsg);

	void handleRecievedMessage();
	void addRecievedMessage(RecievedMessage* recMsg);
	RecievedMessage* buildRecieveMessage(SOCKET sock, int num);

	User* getUserByName(string name);
	User* getUserBySocket(SOCKET sock);
	Room* getRoomById(int id);
};