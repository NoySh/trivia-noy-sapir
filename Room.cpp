#include "Room.h"
#include <algorithm>

Room::Room(int id, User * admin, string name, int maxU, int Qno, int Qtime)
{
	_id = id;
	_admin = admin;
	_name = name; 
	_maxUsers = maxU;
	_qNo = Qno;
	_qTime = Qtime;
	_users.push_back(admin);
}

bool Room::joinRoom(User * user)
{
	string msg;
	if (_maxUsers == _users.size())
	{
		//send error 
		msg = "1100" + _qNo + _qTime;
		sendMessage(user, msg);
		return false;
	}
	else
	{
		_users.push_back(user);
		//send success msg
		msg = "1101" + _qNo + _qTime;
		sendMessage(user, msg);
		for (int i = 0; i < _users.size(); i++)
		{
			sendMessage(_users.at(i), getUsersAsString(_users, user));
		}
		return true;
	}
}

void Room::leaveRoom(User * admin)
{
	string msg;
	if (find(_users.begin(), _users.end(), admin) != _users.end())
	{
		//_users.erase(admin);
		//send success to user for leaving

		msg = "1120";
		sendMessage(admin, msg);
		for (int i = 0; i < _users.size(); i++)
		{
			sendMessage(_users.at(i), getUsersAsString(_users, admin));
		} // update to all the other users in the list
	}
}

int Room::closeRoom(User * admin)
{
	if (_admin == admin)
	{
		//send close room msg to all the users in the room
		//clear room to all the users except the admin
		for (int i = 0; i < _users.size(); i++)
		{
			_users.at(i)->clearRoom();
		}
		return getId();
	}
	else
	{
		return -1;
	}
}

vector<User*> Room::getUsers()
{
	return _users;
}

string Room::getUsersAsString(vector<User*> users, User * user)
{
	return string();
}

void Room::sendMessage(string msg)
{
	sendMessage(nullptr, msg);
}

void Room::sendMessage(User * user, string msg)
{
	try
	{
		user->send(msg);
	}
	catch (exception& e)
	{
		cout << "Error:" << e.what() << endl;
	}
}

string Room::getUsersListMessage()
{
	string msg;
	string usersList = getUsersAsString(_users, this->_admin);
	int numOfUsers = _users.size();
	if (numOfUsers != 0)
	{
		msg = "108##" + to_string(numOfUsers) + "##" + usersList;
	}
	else
	{
		msg = "1080";
	}
	return msg;
}

int Room::getQuestionNo()
{
	return _qNo;
}

int Room::getId()
{
	return _id;
}

string Room::getName()
{
	return _name;
}

User * Room::getAdmin()
{
	return _admin;
}
